﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace http5204_FirstMVC.Migrations
{
    public partial class foreignkeys : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Blogs_Authors_AuthorID",
                table: "Blogs");

            migrationBuilder.DropForeignKey(
                name: "FK_Blogs_Authors_AuthorID1",
                table: "Blogs");

            migrationBuilder.DropForeignKey(
                name: "FK_Pages_Blogs_BlogID",
                table: "Pages");

            migrationBuilder.DropForeignKey(
                name: "FK_Pages_Blogs_BlogID1",
                table: "Pages");

            migrationBuilder.DropIndex(
                name: "IX_Pages_BlogID1",
                table: "Pages");

            migrationBuilder.DropIndex(
                name: "IX_Blogs_AuthorID1",
                table: "Blogs");

            migrationBuilder.DropColumn(
                name: "BlogID1",
                table: "Pages");

            migrationBuilder.DropColumn(
                name: "AuthorID1",
                table: "Blogs");

            migrationBuilder.AlterColumn<int>(
                name: "BlogID",
                table: "Pages",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AuthorID",
                table: "Blogs",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Blogs_Authors_AuthorID",
                table: "Blogs",
                column: "AuthorID",
                principalTable: "Authors",
                principalColumn: "AuthorID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Pages_Blogs_BlogID",
                table: "Pages",
                column: "BlogID",
                principalTable: "Blogs",
                principalColumn: "BlogID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Blogs_Authors_AuthorID",
                table: "Blogs");

            migrationBuilder.DropForeignKey(
                name: "FK_Pages_Blogs_BlogID",
                table: "Pages");

            migrationBuilder.AlterColumn<int>(
                name: "BlogID",
                table: "Pages",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "BlogID1",
                table: "Pages",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AuthorID",
                table: "Blogs",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "AuthorID1",
                table: "Blogs",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Pages_BlogID1",
                table: "Pages",
                column: "BlogID1");

            migrationBuilder.CreateIndex(
                name: "IX_Blogs_AuthorID1",
                table: "Blogs",
                column: "AuthorID1");

            migrationBuilder.AddForeignKey(
                name: "FK_Blogs_Authors_AuthorID",
                table: "Blogs",
                column: "AuthorID",
                principalTable: "Authors",
                principalColumn: "AuthorID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Blogs_Authors_AuthorID1",
                table: "Blogs",
                column: "AuthorID1",
                principalTable: "Authors",
                principalColumn: "AuthorID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Pages_Blogs_BlogID",
                table: "Pages",
                column: "BlogID",
                principalTable: "Blogs",
                principalColumn: "BlogID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Pages_Blogs_BlogID1",
                table: "Pages",
                column: "BlogID1",
                principalTable: "Blogs",
                principalColumn: "BlogID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
