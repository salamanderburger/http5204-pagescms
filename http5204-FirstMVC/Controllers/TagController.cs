﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Net;
using System.Web;
using System.Web.Mvc;
using http5204_FirstMVC.Models;
//including this for the extra definition
using http5204_FirstMVC.Models.ViewModels;
using System.Diagnostics;

namespace http5204_FirstMVC.Controllers
{
    public class TagController : Controller
    {
        BlogCMSContext db = new BlogCMSContext();
        // GET: Tag
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            /*
            Alternate
            IEnumerable<Tag> tags = db.Tags.ToList();
            */

            string query = "select * from Tags";
            IEnumerable<Tag> tags = db.Tags.FromSql(query);
            
            return View(tags);
        }

        public ActionResult New()
        {

            return View();
        }
        [HttpPost]
        public ActionResult Create(string TagName_New, string TagColor_New)
        {
            string query = "insert into tags (TagName, TagColor)" +
                " values (@name, @color)";
            SqlParameter[] myparams = new SqlParameter[2];
            myparams[0] = new SqlParameter("@name", TagName_New);
            myparams[1] = new SqlParameter("@color", TagColor_New);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }

        //TODO: EDIT VIEW, EDIT, DELETE, SHOW

        public ActionResult Edit(int? id)
        {
            if ((id == null) || (db.Tags.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "select * from Tags where tagid=@id";
            SqlParameter param = new SqlParameter("@id",id);
            Tag mytag = db.Tags.FromSql(query, param).FirstOrDefault();
            return View(mytag);
        }

        [HttpPost]
        public ActionResult Edit(int? id, string TagName, string TagColor)
        {
            if ((id == null) || (db.Tags.Find(id) == null)){
                return HttpNotFound();
            }
            string query = "update tags set TagName=@name, TagColor=@color" +
                " where tagid=@id";
            SqlParameter[] myparams = new SqlParameter[3];
            myparams[0] = new SqlParameter("@name",TagName);
            myparams[1] = new SqlParameter("@color",TagColor);
            myparams[2] = new SqlParameter("@id", id);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Show/" + id);
        }

        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if ((id==null)||(db.Tags.Find(id)==null))
            {
                return HttpNotFound();

            }
            string query = "delete from tags where tagid=@id";
            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query,param);
            return View("List");
        }

        public ActionResult Show(int? id)
        {
            if ((id == null) || (db.Tags.Find(id) == null))
            {
                return HttpNotFound();

            }
            string query = "select * from Tags where tagid=@id";
            SqlParameter param = new SqlParameter("@id",id);

            Tag tagtoshow = db.Tags.Include(t=>t.pagesxtags).ThenInclude(pxt=>pxt.Page).SingleOrDefault(t=>t.TagID==id);

            return View(tagtoshow);

        }
    }
}