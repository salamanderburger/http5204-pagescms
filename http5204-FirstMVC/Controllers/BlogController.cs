﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Net;
using System.Web;
using System.Web.Mvc;
using http5204_FirstMVC.Models;
//including this for the extra definition
using http5204_FirstMVC.Models.ViewModels;
using System.Diagnostics;

namespace http5204_FirstMVC.Controllers
{
    public class BlogController : Controller
    {
        private BlogCMSContext db = new BlogCMSContext();

        /*
           Reading links to fully understand this file
           -------------------------------------------



        */


        
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            //DATA NEEDED: All Blogs in DB
            //However, we also have to include the info for the author on each blog

            //[List<Blog> blogs]=> variable named blogs which is a list of Blog
            //[.Include(b=>b.Author)]=> Get the blog's associated author
            //[.ToList()]=>Return a list of information
            List<Blog> blogs = db.Blogs.Include(b=>b.Author).ToList();

            //GOTO Views/Blog/List.cshtml
            return View(blogs);
        }

        public ActionResult New()
        {
            //DATA NEEDED
            //We only need a list of authors to populate the dropdownlist

            //Model defined in Models/ViewModels/BlogEdit.cs
            BlogEdit blogeditview = new BlogEdit();

            //[blogeditview.authors]=> Set the author field of the BlogEdit Object
            //[db.Authors.ToList()]=> Get all authors from DB
            blogeditview.Authors = db.Authors.ToList();

            //GOTO Views/Blog/New.cshtml
            return View(blogeditview);
        }

        [HttpPost]
        public ActionResult Create(string BlogTitle_New, int BlogAuthor_New, string BlogBio_New)
        {
            //Data Needed: None

            //Raw Query   
            string query = "insert into blogs (BlogTitle, BlogBio, AuthorID) values (@title, @bio, @author)";

            //SQL parameterized query technique
            //[SqlParameter[] myparams] => SET variable myparams as an array of type (SqlParameter)
            //[new SqlParameter[1]] => Create a new SqlParameter array with 3 items
            SqlParameter[] myparams = new SqlParameter[3];
            //@title paramter
            myparams[0] = new SqlParameter("@title", BlogTitle_New);
            //@bio parameter
            myparams[1] = new SqlParameter("@bio", BlogBio_New);
            //@author (id) FOREIGN KEY paramter
            myparams[2] = new SqlParameter("@author", BlogAuthor_New);
            
            //Run the parameterized query (DML - Data Manipulation Language)
            //Insert into blogs ( .. ) values ( .. ) 
            db.Database.ExecuteSqlCommand(query, myparams);

            //GOTO: 
            return RedirectToAction("List");
        }

        public ActionResult Edit(int id)
        {
            //Data Needed:
            //One particular blog (we have the id)
            //That blogs' author
            //All other authors in DB (for dropdownlist)

            //Model defined in Models/ViewModels/BlogEdit.cs
            BlogEdit blogeditview = new BlogEdit();

            //[blogeditview.authors]=>SET authors field of BlogEdit Object
            //[ db.Authors.ToList()]=>GET authors from DB into List
            blogeditview.Authors = db.Authors.ToList(); //Finds all the authors

            //[blogeditview.blog]=>SET blog field of BlogEdit Object
            //[db.Blogs]=> GET blogs from DB
            //[.Include(b => b.Author)]=> JOIN that blogs author
            //[.SingleOrDefault(b=>b.BlogID==id)]=> WHERE blogid = id
            blogeditview.Blog = db.Blogs.Include(b => b.Author).SingleOrDefault(b=>b.BlogID==id); //finds all the blogs

            //GOTO: Views/Blog/Edit.cshtml
            return View(blogeditview);
        }

        [HttpPost]
        public ActionResult Edit(int id,string BlogTitle, int BlogAuthor, string BlogBio)
        {
            //[(id == null)] => No ID passed to GET (ie. Blog/Edit) instead of (Blog/Edit/3)
            //[||]=> OR logical operator
            //[(db.Blogs.Find(id) == null)] => No blog found in DB
            if ((id == null) || (db.Blogs.Find(id) == null))
            {
                return HttpNotFound();

            }
            //Raw Update MSSQL query
            string query = "update blogs set BlogTitle=@title, BlogBio=@bio, AuthorID=@author where blogid=@id";

            //[SqlParameter[] myparams] => SET variable myparams as an array of type (SqlParameter)
            //[new SqlParameter[1]] => Create a new SqlParameter array with 4 items
            SqlParameter[] myparams = new SqlParameter[4];
            //Parameter for @title "blogtitle"
            myparams[0] = new SqlParameter("@title", BlogTitle);
            //Parameter for @bio "blogbio"
            myparams[1] = new SqlParameter("@bio", BlogBio);
            //Parameter for (author) id FOREIGN KEY
            myparams[2] = new SqlParameter("@author", BlogAuthor);
            //Pararameter for (blog) id PRIMARY KEY
            myparams[3] = new SqlParameter("@id", id);
            
            //Execute the custom SQL command with parameters
            db.Database.ExecuteSqlCommand(query, myparams);

            //GOTO: View/Blog/Show.cshtml with paramter Blogid passed
            return RedirectToAction("Show/"+id);
        }

        public ActionResult Show(int? id)
        {
            //[(id == null)] => No ID passed to GET (ie. Blog/Edit) instead of (Blog/Edit/3)
            //[||]=> OR logical operator
            //[(db.Blogs.Find(id) == null)] => No blog found in DB
            if ((id == null) || (db.Blogs.Find(id)==null))
            {
                return HttpNotFound();

            }
            //Raw MSSQL query
            string query = "select * from blogs where blogid=@id";

            //[SqlParameter[] myparams] => SET variable myparams as an array of type (SqlParameter)
            //[new SqlParameter[1]] => Create a new SqlParameter array with 1 item
            SqlParameter[] myparams = new SqlParameter[1];
            myparams[0] = new SqlParameter("@id", id);

            //Hey view.. here's a list of all the blogs in my db
            //include the pages I need
            //include the author (so I can see who wrote it)
            Blog myblog = db.Blogs.FromSql(query, myparams).Include(b=>b.Author).Include(b=>b.Pages).FirstOrDefault();

            return View(myblog);
        }
        
        public ActionResult Delete(int? id)
        {
            //[(id == null)] => No ID passed to GET (ie. Blog/Edit) instead of (Blog/Edit/3)
            //[||]=> OR logical operator
            //[(db.Blogs.Find(id) == null)] => No blog found in DB
            if ((id == null) || (db.Blogs.Find(id) == null))
            {
                return HttpNotFound();

            }
            //These three statements should definitely be wrapped in
            //a stored procedure instead of me manually doing it
            //key term here "referential integrity"

            //delete pagesxtags associated with pages associated with blogs
            string query = "delete from PagesxTags where PageID in (select PageID from pages where BlogID=@id)";
            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);

            //then delete associated pages
            query = "delete from pages where BlogID=@id";
            param = new SqlParameter("@id",id);
            db.Database.ExecuteSqlCommand(query,param);
            
            //finally delete blog
            query = "delete from Blogs where blogid=@id";
            param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);
            return RedirectToAction("List");
        }

    }
}