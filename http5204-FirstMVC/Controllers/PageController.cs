﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Net;
using System.Web;
using System.Web.Mvc;
using http5204_FirstMVC.Models;
//including this for the extra definition
using http5204_FirstMVC.Models.ViewModels;
using System.Diagnostics;

namespace http5204_FirstMVC.Controllers
{
    public class PageController : Controller
    {
        //variable which holds our database
        private BlogCMSContext db = new BlogCMSContext();

        /*
           Reading links to fully understand this file
           -------------------------------------------



        */



        // GET: Page
        public ActionResult Index()
        {
            //Redirect to the list view method
            return RedirectToAction("List");
        }

        public ActionResult New()
        {
            
            return View(db.Blogs.ToList());
        }

        //Restricts this method to only handle POST
        //eg. POST to /Pages/Create/
        [HttpPost]
        public ActionResult Create(string PageTitle_New, string PageContent_New, int? PageBlog_New)
        {
            string query = "insert into Pages (PageTitle, PageContent, BlogID) " +
                "values (@title,@content,@bid)";

            SqlParameter[] myparams = new SqlParameter[3];
            myparams[0] = new SqlParameter("@title",PageTitle_New);
            myparams[1] = new SqlParameter("@content",PageContent_New);
            myparams[2] = new SqlParameter("@bid", PageBlog_New);
           
            db.Database.ExecuteSqlCommand(query,myparams);
            //testing that the paramters do indeed pass to the method
            //Debug>Windows>Output
            Debug.WriteLine(query);
            return RedirectToAction("List");
        }

        public ActionResult Show(int id)
        {
            //raw query
            string query = "select * from pages where pageid =@id";
            
            //Debug>Windows>Output
            Debug.WriteLine(query);

            //DATA NEEDED: page with it's tags and also the author
            //[db.Pages.FromSql(query, new SqlParameter("@id",id))]=>Execute raw SQL (above)
            //[.Include(p=>p.Blog.Author)]=> Include the Pages Blog so that we can get the Blogs Author
            //[.Include(p=>p.pagesxtags)]=> Include the tag id bridging data
            //[.ThenInclude(pxt => pxt.Tag)] => Include actual tag information after we have id bridging data
            //[.SingleOrDefault(p=>p.PageID==id))]=> Get the one where the pageid is the id 
            return View(db.Pages.FromSql(query, new SqlParameter("@id",id)).Include(p=>p.Blog.Author).Include(p => p.pagesxtags).ThenInclude(pxt => pxt.Tag).SingleOrDefault(p=>p.PageID==id));
        }

        //This shows the edit interface
        //The edit interface is more advanced with a relational record now.
        public ActionResult Edit(int? id)
        {
            //DATA NEEDED: page data (with specific blog and tags), all blog data, all tag data
            //Modern Technique
            //Make a new compound view which holds the data we need 
            //to display page edit
            PageEdit pageedit = new PageEdit();
            //[db.Pages]=> get page data.
            //[.Include(p=>p.pagesxtags)]=> include (tags) data for page.
            //[.SingleOrDefault(p => p.PageID == id)]=> include the pages' blog. Take the one where the ID matches GET parameter
            pageedit.Page = db.Pages.Include(p=>p.pagesxtags).Include(p=>p.Blog).SingleOrDefault(p => p.PageID == id);
            //get all blog data
            pageedit.Blogs = db.Blogs.ToList();
            //get all tag data
            pageedit.Tags = db.Tags.ToList();
            //if we have info, pass it to Page/View.cshtml
            if (pageedit.Page != null) return View(pageedit);
            else return HttpNotFound();
        }
        //This one actually does the editing commmand
        [HttpPost]
        public ActionResult Edit(int? id, string PageTitle, string PageContent, int? PageBlog, int?[] PageTags, int?[] Removed_PageTags)
        {
            //[(id == null) ]=>no ID in GET
            //[(db.Pages.Find(id) == null)]=>couldn't find this page
            if ((id == null) || (db.Pages.Find(id) == null))
            {
                //Show error message
                return HttpNotFound();

            }
            //Raw query data
            string query = "update pages set pagetitle=@title, " +
                "pagecontent=@content, " +
                "BlogID=@bid where pageid = @id";

            //parameter for @title
            SqlParameter[] myparams = new SqlParameter[4];
            myparams[0] = new SqlParameter();
            myparams[0].ParameterName = "@title";
            myparams[0].Value = PageTitle;

            //parameter for @content
            myparams[1] = new SqlParameter();
            myparams[1].ParameterName = "@content";
            myparams[1].Value = PageContent;

            //parameter for @big (blogid) FOREIGN KEY
            myparams[2] = new SqlParameter();
            myparams[2].ParameterName = "@bid";
            myparams[2].Value = PageBlog;

            //parameter for @id (pageid) PRIMARY KEY
            myparams[3] = new SqlParameter();
            myparams[3].ParameterName = "@id";
            myparams[3].Value = id;         
            db.Database.ExecuteSqlCommand(query, myparams);

            //[(PageTags!=null)]=> No page tags found (GET parameter, result of checkboxes)
            //[(PageTags.Count() > 0)]=> No pages found in DB 
            if ((PageTags!=null) && (PageTags.Count() > 0))
            {
                //Go through each tag that the user wanted to add
                foreach (int tagid in PageTags)
                {
                    //If this page tag doesn't exist we skip
                    if (db.Tags.Find(tagid) == null) continue;

                    //I cannot enter a PagexTag record if it already exists.
                    if (db.Pages.Include(p => p.pagesxtags).SingleOrDefault(p => p.PageID == id).pagesxtags.Exists(pxt => pxt.Tagid == tagid)) continue;
                    
                    //also note I use tagquery and mytagparams as variable names
                    //so they don't collide with the ones above
                    string tagquery = "insert into PagesxTags (TagID,PageID) values (@tid,@pid)";
                    
                    Debug.WriteLine("I'm trying to insert a record " +
                        "into tagpages with tagid of " + tagid+
                        "and pageid of "+id);
                    
                    SqlParameter[] mytagparams = new SqlParameter[2];
                    mytagparams[0] = new SqlParameter("@tid", tagid);
                    mytagparams[1] = new SqlParameter("@pid", id);
                    db.Database.ExecuteSqlCommand(tagquery, mytagparams);
                }
            }
            //This code is the antonym of the code above
            //These checkboxes are reverse populated onchange in JS (see Page/Edit.cshtml)
            if ((Removed_PageTags != null) && (Removed_PageTags.Count() > 0))
            {
                //Go through each tag that is to be removed
                foreach (int tagid in Removed_PageTags)
                {
                    //This is a very similar codeblock as before but the exact opposite
                    //we are removing any page tags in this list.

                    //If this page tag doesn't exist we skip
                    if (db.Tags.Find(tagid) == null) continue;

                    //If a page has no tags, skip (none to remove)
                    if (db.Pages.Include(p=>p.pagesxtags).SingleOrDefault(p=>p.PageID==id).pagesxtags.Count()==0) continue;
                    
                    //is the page NOT already associated with this tag? if so, skip
                    if (!db.Pages.Include(p=>p.pagesxtags).SingleOrDefault(p=>p.PageID==id).pagesxtags.Exists(pxt => pxt.Tagid == tagid)) continue;

                    string tagremovequery = "delete from PagesxTags where Tagid=@tid and PageID=@pid";
                    //Debug>Windows>Output
                    /*
                    Debug.WriteLine("I'm trying to remove a record " +
                        "from tagpages with tagid of " + tagid +
                        "and pageid of " + id);
                    */
                    SqlParameter[] mytagrmparams = new SqlParameter[2];
                    //tag id parameter
                    mytagrmparams[0] = new SqlParameter("@tid", tagid);
                    //page id parameter
                    mytagrmparams[1] = new SqlParameter("@pid", id);
                    db.Database.ExecuteSqlCommand(tagremovequery, mytagrmparams);

                }
            }
            //GOTO: SHOW method in PageController.cs and pass argument (page)id
            return RedirectToAction("Show/" + id);
        }

        //GET of localhost/pages/delete/2 implies
        //delete action on pages controller with id 2
        public ActionResult Delete(int id)
        {
            //Establish referential integrity first
            string query = "delete from PagesxTags where Pageid = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

            //Then delete main record
            query = "delete from pages where pageid = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id",id));

            //GOTO: method List in PageController.cs
            return RedirectToAction("List");
        }

        public ActionResult List()
        {

            //Data Needed: All Pages and their tags and their author
            //[db.Pages]=>all pages in DB
            //[.Include(p=>p.Blog.Author)] => take that pages authors (display authorname)
            //[.Include(p=>p.pagesxtags)] => get bridging data (which tagid)
            //[.ThenInclude(pxt=>pxt.Tag)] = >get the actual tag data (like color, name)
            //[.ToList()]=>Return this info as a list
            
            return View(db.Pages.Include(p=>p.Blog.Author).Include(p=>p.pagesxtags).ThenInclude(pxt=>pxt.Tag).ToList());
            
        }
        
    }
}