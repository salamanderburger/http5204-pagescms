﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using http5204_FirstMVC.Models;
using http5204_FirstMVC.Models.ViewModels;
using System.Diagnostics;
using System.IO;

namespace http5204_FirstMVC.Controllers
{
    public class SearchController : Controller
    {

        //This is the controller which primarily deals with LIST
        private BlogCMSContext db = new BlogCMSContext();

        public ActionResult Explore()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Results(string searchkey)
        {
            //SearchItem is defined in Model/SearchItem.cs
            //SearchItems is referenced in Models/BlogCMSContext.cs
            //SearchItems is of type <DbQuery>, as of EF Core 2.1 can be used to map to non-entity types
            //What does that mean? It means I can build an arbitrary class (searchitem) and map results to it
            //The last piece of the puzzles is the mapping between column names and class fields
            //check the DATABASE VIEWS for a view of the same name as the DbQuery variable (SearchItems)


            var rawdata =
                db.SearchItems
                .Where(si =>
                    EF.Functions.Like(si.SearchTitle, "%" + searchkey + "%")
                    ||EF.Functions.Like("%"+searchkey+"%", si.SearchTitle)
                    || EF.Functions.Like(si.SearchContent, "%" + searchkey + "%")
                    || EF.Functions.Like("%" + searchkey + "%", si.SearchContent))
                .ToList();



            return View(rawdata);
            
        }

    }
}