﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace http5204_FirstMVC.Models
{
    public class PagexTag
    {
        //standard bridging table, just required to be implemented in EF core
        public int PageID { get; set; }
        public Page Page { get; set; }

        public int Tagid { get; set; }
        public Tag Tag { get; set; }
    }
}