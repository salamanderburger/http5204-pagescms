﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace http5204_FirstMVC.Models
{
    public class Blog
    {
        //One blog to many articles

        [Key,ScaffoldColumn(false)]
        public int BlogID { get; set; }

        [Required, StringLength(255), Display(Name = "Title")]
        public string BlogTitle { get; set; }

        [StringLength(int.MaxValue), Display(Name = "Content")]
        public string BlogBio { get; set; }

        //This is how we can represent a one (blog) to many (pages) relation
        //notice how if we were using a relational database this column
        //would be included as a foreign of authorid in the pages table.
        [InverseProperty("Blog")]
        public virtual List<Page> Pages { get; set; }

        
        //blog has author ID
        [ForeignKey("AuthorID")]
        public int AuthorID { get; set; }
        //Blog Author
        public virtual Author Author { get; set; }
        

    }
}