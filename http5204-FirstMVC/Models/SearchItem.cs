﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.EntityFrameworkCore;

namespace http5204_FirstMVC.Models
{
    public class SearchItem
    {
        //A search item is going to be an item in a list
        //This is information we present as a search result

        //title
        public string SearchTitle { get; set; }
        //content
        public string SearchContent { get; set; }
        
        //type
        public string SearchType { get; set; }
        //id
        public int SearchID { get; set; }




    }
}