﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Data.Entity;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace http5204_FirstMVC.Models
{
    public class Author
    {
        [Key]
        public int AuthorID { get; set; }

        [Required, StringLength(255), Display(Name = "First Name")]
        public string AuthorFName { get; set; }

        [Required, StringLength(255), Display(Name = "Last Name")]
        public string AuthorLName { get; set; }

        [Required, StringLength(255), Display(Name = "Pen Name")]
        public string AuthorPenName { get; set; }

        //profile picture
        //1=>picture exists (in imgs/authors/{id}.img)
        //0=>picture doesn't exist
        public int HasPic { get; set; }

        //Accepted image formats (jpg/jpeg/png/gif)
        public string ImgType { get; set; }

        //This is how we can represent a one (author) to many (Blogs) relation
        //notice how if we were using a relational database this column
        //would be included as a foreign of authorid in pages.
        [InverseProperty("Author")]
        public List<Blog> Blogs { get; set; }


    }

    
}