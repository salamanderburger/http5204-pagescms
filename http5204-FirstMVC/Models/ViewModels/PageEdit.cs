﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace http5204_FirstMVC.Models.ViewModels
{
    public class PageEdit
    {

        //Empty constructor
        public PageEdit()
        {

        }

        //Raw page information (in Models/Page.cs)
        public virtual Page Page { get; set; }
        
        //need information about the different blogs this page COULD be
        //assigned to
        public IEnumerable<Blog> Blogs { get; set; }

        //need information about the different tags this page COULD be
        //assigned to
        public IEnumerable<Tag> Tags { get; set; }
    }
}