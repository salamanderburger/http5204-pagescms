﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace http5204_FirstMVC.Models.ViewModels
{
    public class BlogEdit
    {
        //Empty constructor
        public BlogEdit()
        {

        }

        //To edit a blog, you also need to pick from a list of authors

        public virtual Blog Blog { get; set; }

        public IEnumerable<Author> Authors { get; set; }
    }
}