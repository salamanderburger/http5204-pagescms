﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace http5204_FirstMVC.Models
{
    public class Page
    {
        //One author to many pages
        //One blog to many pages

        //The stuff above the field definitions are called DataAnnotations
        //They let the App know what kind of data it is, and how to present it
        [Key, ScaffoldColumn(false)]
        public int PageID { get; set; }

        //required string length up to 255 characters,
        //labelled as "Page Name"
        [Required, StringLength(255), Display(Name = "Title")]
        public string PageTitle { get; set; }

        //Page Content.
        [StringLength(int.MaxValue), Display(Name = "Content")]
        public string PageContent { get; set; }

        
        [ForeignKey("BlogID")]
        public int BlogID { get; set; }
        //One blog to many pages
        public virtual Blog Blog { get; set; }

        //many tags to many pages
        [InverseProperty("Page")]
        public virtual List<PagexTag> pagesxtags {get;set;}
        
    }
}