﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//Install "Entity Framework"
//Right click solution > Manage NuGet Packages
//Search Entity Framework and install to solution
//using System.Data.Entity;
//using System.Data.Entity.Infrastructure;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;

using System.Configuration;

//This is a Model. What we're doing is defining classes
//That will define our database and tables.

namespace http5204_FirstMVC.Models
{
    //SubClass of DbContext
    public class BlogCMSContext : DbContext
    {
        
        public BlogCMSContext()
        {

        }
        

        public DbSet<Page> Pages { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Tag> Tags { get; set; }

        //Search result
        public DbQuery<SearchItem> SearchItems { get; set; }
        public DbQuery<PageSearch> PageSearch { get; set; }

        //Entity framework core asks me to specify sql server
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConfigurationManager.ConnectionStrings["CoreBlogCMS"].ConnectionString);
        }

        //also need to specify manyxmany relationships in this method

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //describing that a pagextag PK is composite of page and tag
            modelBuilder.Entity<PagexTag>()
                .HasKey(pxt => new { pxt.PageID, pxt.Tagid});

            //describing that pagextag is associated with one page
            //AND one page has many pagesxtags
            //AND pagesxtags has foreign key to pageid
            modelBuilder.Entity<PagexTag>()
                .HasOne(pxt => pxt.Page)
                .WithMany(pxt => pxt.pagesxtags)
                .HasForeignKey(pxt => pxt.PageID);

            //Describing that pagextag is associated to one tag
            //AND one tag has many pagesxtags
            //and pagesxtags has foreign key to tagid
            modelBuilder.Entity<PagexTag>()
                .HasOne(pxt => pxt.Tag)
                .WithMany(pxt => pxt.pagesxtags)
                .HasForeignKey(pxt => pxt.Tagid);

            //Author has many blogs, each blog has one author
            modelBuilder.Entity<Blog>()
                .HasOne(b=>b.Author)
                .WithMany(a=>a.Blogs)
                .HasForeignKey(b=>b.AuthorID);

            //blog has many pages, each page has one blog
            modelBuilder.Entity<Page>()
                .HasOne(p => p.Blog)
                .WithMany(b=>b.Pages)
                .HasForeignKey(p=>p.BlogID);

            base.OnModelCreating(modelBuilder);
            //also need to specify that these models make tables
            modelBuilder.Entity<Blog>().ToTable("Blogs");
            modelBuilder.Entity<Page>().ToTable("Pages");
            modelBuilder.Entity<Tag>().ToTable("Tags");
            modelBuilder.Entity<Author>().ToTable("Authors");
            modelBuilder.Entity<PagexTag>().ToTable("PagesxTags");

            //This is for search functionality




        }


}
}