﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace http5204_FirstMVC.Models
{
    public class Tag
    {
        [Key,ScaffoldColumn(false)]
        public int TagID { get; set; }

        [Required, StringLength(255), Display(Name = "Name")]
        public string TagName { get; set; }

        [Required, StringLength(255), Display(Name = "Color")]
        public string TagColor { get; set; }

        //many tags to many pages
        public virtual ICollection<PagexTag> pagesxtags { get; set; }

    }
}